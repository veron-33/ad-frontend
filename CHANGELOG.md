
# 07.02.2014
* Add favicon.ico
* UI-fix: remove z-index of menu-bar
* UI-fix: restyle text-imputs in new_user_popup
* Fix bug of wrong height of the ldap-tree